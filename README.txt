This is a Confluence plugin that demonstrates how to use JavaScript to 
invoke an XWork action from a web-item without causing the browser to 
navigate to a new page.

Tested against: Confluence 5.1.4
On Date: Tuesday July 23 2013
