// use toInit to delay running this code until after the page has finished loading
AJS.toInit(function() {

    // the "linkId" in the atlassian-plugin.xml for the web-item will become the unique ID of the <a> element in the DOM
    // for the WebItem, so we can use JQuery selectors identify it.
    AJS.$("#example-webitem-link").click(function(e) {
        // Run some code when the link is clicked.
        var link$ = AJS.$(this);

        // Extract the "href" attribute from the link and use JQuery to issue a HTTP GET request.
        AJS.$.get(link$.prop("href"), function(data) {
            // The response from the action will be some JSON data. Print it to a pop-up window.
            alert("The XWork action said: " + data);
        });

        // Tell the browser not to follow the clicked link; stay on the current page.
        e.preventDefault();
    });
});