package com.atlassian.confluence.test;

import com.atlassian.confluence.core.Beanable;
import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.opensymphony.xwork.Action;

public class ExampleAction extends ConfluenceActionSupport implements Beanable // Implement the Beanable interface to get the "getBean" method below
{
    @Override
    public String execute() throws Exception
    {
        // This is just a simple example, so we have no work to do... just return "success".
        return Action.SUCCESS;
    }

    @Override
    public Object getBean()
    {
        // Use the "getBean" method to return some data to the browser - it will get converted to JSON, since that's
        // how the XWork action is configured in the atlassian-plugin.xml
        return "Hello, AJAX World!";
    }
}
